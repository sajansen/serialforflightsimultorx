import unittest

import fsx, status
from config import Config
from fsx import FSX


class TestFsx(unittest.TestCase):

  def setUp(self) -> None:
    status.Status.TEST_STATUS_1 = 0
    status.Status.TEST_STATUS_2 = 2
    status.Status.TEST_STATUS_1_BIT = 3
    status.Status.TEST_STATUS_2_BIT = 4
    status.Status.TEST_STATUS_3_BIT = 5
    status.Status.TEST_STATUS_4_BIT = 6

    status.status_params = {
      status.Status.TEST_STATUS_1: {'address': 0x0001, 'byte_type': 'U8'},
      status.Status.TEST_STATUS_2: {'address': 0x0002, 'byte_type': 'U16'},
      status.Status.TEST_STATUS_1_BIT: {'address': 0x0003, 'byte_type': 'U16', 'bit_offset': 0},
      status.Status.TEST_STATUS_2_BIT: {'address': 0x0004, 'byte_type': 'U16', 'bit_offset': 1},
      status.Status.TEST_STATUS_3_BIT: {'address': 0x0003, 'byte_type': 'U16', 'bit_offset': 3},
      status.Status.TEST_STATUS_4_BIT: {'address': 0x0003, 'byte_type': 'U16', 'bit_offset': 2},
    }

    self.simulator = FSX()
    fsx.pyuipc.prepare_data = lambda x: [address for address, byte_type in x]
    fsx.pyuipc.read = self.mock_pyuipc_read_for_test_get_status

  def test_get_status_values_for_non_bit_status_details(self):
    non_bit_statuses_details = {
      status.Status.TEST_STATUS_1: {'address': 0x0001, 'byte_type': 'U8'},
      status.Status.TEST_STATUS_2: {'address': 0x0002, 'byte_type': 'U16'},
    }

    self.simulator.get_status_values_for_non_bit_status_details(non_bit_statuses_details)

    self.assertEqual(200, non_bit_statuses_details.get(status.Status.TEST_STATUS_1).get('value'))
    self.assertEqual(12, non_bit_statuses_details.get(status.Status.TEST_STATUS_2).get('value'))

  def test_get_status_values_for_bit_status_details(self):
    bit_statuses_details = {
      status.Status.TEST_STATUS_1_BIT: {'address': 0x0003, 'byte_type': 'U16', 'bit_offset': 0},
      status.Status.TEST_STATUS_2_BIT: {'address': 0x0004, 'byte_type': 'U16', 'bit_offset': 1},
      status.Status.TEST_STATUS_3_BIT: {'address': 0x0003, 'byte_type': 'U16', 'bit_offset': 3},
      status.Status.TEST_STATUS_4_BIT: {'address': 0x0003, 'byte_type': 'U16', 'bit_offset': 2},
    }

    self.simulator.get_status_values_for_bit_status_details(bit_statuses_details)

    self.assertEqual(0, bit_statuses_details.get(status.Status.TEST_STATUS_1_BIT).get('value'))
    self.assertEqual(0, bit_statuses_details.get(status.Status.TEST_STATUS_2_BIT).get('value'))
    self.assertEqual(0, bit_statuses_details.get(status.Status.TEST_STATUS_3_BIT).get('value'))
    self.assertEqual(1, bit_statuses_details.get(status.Status.TEST_STATUS_4_BIT).get('value'))

  def test_get_status(self):
    # Given
    Config.simulation_to_serial_statuses = [
      status.Status.TEST_STATUS_1,
      status.Status.TEST_STATUS_1_BIT,
      status.Status.TEST_STATUS_2_BIT,
      status.Status.TEST_STATUS_4_BIT,
      status.Status.TEST_STATUS_2,
      status.Status.TEST_STATUS_3_BIT,
    ]

    simulator = FSX()

    # When
    values = simulator.get_status()

    self.assertEqual([200, 0, 0, 1, 12, 0], values)

  @staticmethod
  def mock_pyuipc_read_for_test_get_status(input_addresses):
    return [TestFsx.get_read_value_for_address(address) for address in input_addresses]

  @staticmethod
  def get_read_value_for_address(input_addresses):
    if input_addresses == 0x0001:
      return 200
    if input_addresses == 0x0002:
      return 12
    if input_addresses == 0x0003:
      return int('00000110', 2)
    if input_addresses == 0x0004:
      return int('00000101', 2)
    raise ValueError("Unknown address given to read")

    
    
    