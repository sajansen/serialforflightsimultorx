import unittest

import status
from config import Config


class TestStatus(unittest.TestCase):

  def setUp(self) -> None:
    status.Status.TEST_STATUS_1 = 0
    status.Status.TEST_STATUS_2 = 1
    Config.COM_read_byte_order = 'big'
    Config.COM_write_byte_order = 'big'

  def test_get_byte_length_for_status_offset(self):
    result = status.get_bytetype_byte_length('x32')
    self.assertEqual(result, 4)

    result = status.get_bytetype_byte_length('x16')
    self.assertEqual(result, 2)

    result = status.get_bytetype_byte_length('x8')
    self.assertEqual(result, 1)

  def test_is_status_a_bit_value(self):
    status.status_params = {
      status.Status.TEST_STATUS_1: {'bit_offset': 0},
      status.Status.TEST_STATUS_2: {},
    }

    self.assertTrue(status.is_status_a_bit_value(status.Status.TEST_STATUS_1))
    self.assertFalse(status.is_status_a_bit_value(status.Status.TEST_STATUS_2))

  def test_get_status_offset(self):
    status.status_params = {
      status.Status.TEST_STATUS_1: {'address': 0x0001, 'byte_type': 'S32'},
      status.Status.TEST_STATUS_2: {'address': 0x0001, 'byte_type': 'S32'},
    }

    offset = status.get_status_params(status.Status.TEST_STATUS_2)

    self.assertEqual(offset, {'address': 0x0001, 'byte_type': 'S32'})

  def test_get_status_offset_withNonExistingStatusOffset(self):
    status.status_params = {
      status.Status.TEST_STATUS_1: {'address': 0x0001, 'byte_type': 'S32'},
    }

    with self.assertRaises(IndexError):
      status.get_status_params(status.Status.TEST_STATUS_2)

  def test_status_valus_as_bytes_withSmallValues(self):
    status.status_params = {
      status.Status.TEST_STATUS_1: {'address': 0x0001, 'byte_type': 'U8'},
    }
    value = 9

    result = status.status_valus_as_bytes(status.Status.TEST_STATUS_1, value)

    self.assertEqual(result, b'\x09')

  def test_status_valus_as_bytes_withLargeValues(self):
    status.status_params = {
      status.Status.TEST_STATUS_1: {'address': 0x0001, 'byte_type': 'U32'},
    }
    value = 123456

    result = status.status_valus_as_bytes(status.Status.TEST_STATUS_1, value)

    self.assertEqual(result, b'\x00\x01\xe2\x40')

  def test_status_valus_as_bytes_withSignedValues(self):
    status.status_params = {
      status.Status.TEST_STATUS_1: {'address': 0x0001, 'byte_type': 'S16'},
    }
    value = -12345

    result = status.status_valus_as_bytes(status.Status.TEST_STATUS_1, value)

    self.assertEqual(result, b'\xcf\xc7')

  def test_statuses_to_bytes(self):
    Config.COM_read_byte_order = 'little'
    status.status_params = {
      status.Status.TEST_STATUS_1: {'address': 0x0001, 'byte_type': 'U8'},
      status.Status.TEST_STATUS_2: {'address': 0x0002, 'byte_type': 'U16'},
    }
    Config.simulation_to_serial_statuses = [
      status.Status.TEST_STATUS_1,
      status.Status.TEST_STATUS_2
    ]

    result = status.statuses_to_bytes([9, 290])

    self.assertEqual(result, b'\x09\x22\x01')


