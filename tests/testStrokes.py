import logging
import unittest
from unittest.mock import Mock, call

import strokes, utils

from config import Config


class TestMain(unittest.TestCase):
  @classmethod
  def setUpClass(cls):
    utils.setup_logging()

  def setUp(self):
    strokes.pyautogui = Mock()

  def test_get_strokes_for_value_ThrowsExceptionOnInvalidKey(self):
    Config.keystroke_mappings = {
      '00': ['a']
    }

    with self.assertRaises(IndexError):
      strokes.get_strokes_for_value('01')

  def test_executeStrokeForValueExecutesStrokesForKey(self):
    Config.keystroke_mappings = {
      '00': ['a'],
      '01': ['b']
    }

    strokes.execute_stroke_for_value('00')

    strokes.pyautogui.press.assert_called_once_with('a')
    strokes.pyautogui.hotkey.assert_not_called()

  def test_executeStrokeForValueExecutesStrokesForSpecialCharKey(self):
    Config.keystroke_mappings = {
      '00': ['a'],
      b'\x09': ['b']  # Also known as '\t'
    }

    strokes.execute_stroke_for_value(b'\t')

    strokes.pyautogui.press.assert_called_once_with('b')
    strokes.pyautogui.hotkey.assert_not_called()

  def test_executeStrokeForValueExecutesHotkeyStrokesForKey(self):
    Config.keystroke_mappings = {
      '00': [['ctrl', 'a']]
    }

    strokes.execute_stroke_for_value('00')

    strokes.pyautogui.press.assert_not_called()
    strokes.pyautogui.hotkey.assert_called_once_with('ctrl', 'a')

  def test_executeStrokeForValueExecutesHotkeyAndNormalStrokesForKey(self):
    Config.keystroke_mappings = {
      '00': ['a', ['ctrl', 'a'], 'b', ['ctrl', 'c']]
    }

    strokes.execute_stroke_for_value('00')

    strokes.pyautogui.press.assert_has_calls([call('a'), call('b')])
    strokes.pyautogui.hotkey.assert_has_calls([call('ctrl', 'a'), call('ctrl', 'c')])
