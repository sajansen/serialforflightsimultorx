import unittest

import utils
from config import Config


class TestConfig(unittest.TestCase):
  @classmethod
  def setUpClass(cls):
    utils.setup_logging()

  def setUp(self):
    Config.COM_needs_status_update_byte = 'xx'

  def test_hex_string_to_byte(self):
    self.assertEqual(Config.hex_string_to_key("0x00"), 0)
    self.assertEqual(Config.hex_string_to_key("0x01"), 1)
    self.assertEqual(Config.hex_string_to_key("0xfa"), 250)

  def test_string_key_to_int_key(self):
    self.assertEqual(0, Config.string_key_to_int_key('0x00'))
    self.assertEqual(0, Config.string_key_to_int_key('0'))
    self.assertEqual(0, Config.string_key_to_int_key(0))
    self.assertEqual(3, Config.string_key_to_int_key('0x03'))
    self.assertEqual(3, Config.string_key_to_int_key('3'))
    self.assertEqual(3, Config.string_key_to_int_key(3))
    self.assertEqual(250, Config.string_key_to_int_key('0xfa'))
    self.assertEqual(250, Config.string_key_to_int_key('250'))
    self.assertEqual(250, Config.string_key_to_int_key(250))

  def test_load_config_file_withValidContent(self):
    Config.COM_port = "COM0"
    Config.COM_baud_rate = 9600

    # when
    Config.load_config_file('resources/testConfigYaml_valid.yaml')

    # then
    self.assertEqual(Config.COM_port, "COM1")
    self.assertEqual(Config.COM_baud_rate, 9600)

  def test_load_config_file_withInValidContent(self):
    Config.COM_port = "COM0"
    Config.COM_baud_rate = 9600

    # when
    Config.load_config_file('resources/testConfigYaml_invalid.yaml')

    # then
    self.assertEqual(Config.COM_port, "COM1")
    self.assertEqual(Config.COM_baud_rate, 9600)
    self.assertFalse(hasattr(Config, 'invalidstuff'))

  def test_load_config_file_withInValidSyntax(self):
    Config.COM_port = "COM0"
    Config.COM_baud_rate = 9600

    # when
    with self.assertRaises(Exception):
      Config.load_config_file('resources/testConfigYaml_invalidSyntax.yaml')

    # then
    self.assertEqual(Config.COM_port, "COM0")
    self.assertEqual(Config.COM_baud_rate, 9600)

  def test_load_config_file_withInValidUseOfReservedKey(self):
    Config.keystroke_mappings = {}
    Config.COM_needs_status_update_byte = '00'

    # when
    with self.assertRaises(KeyError):
      Config.load_config_file('resources/testConfigYaml_invalidUseOfReservedKey.yaml')

    # then
    self.assertTrue('00' not in Config.keystroke_mappings.keys())

  def test_setup_config_withOutFile(self):
    Config.COM_port = "COM0"
    Config.COM_baud_rate = 9600

    # when
    Config.setup_config(config_file_path='somwhere/over/the/rainbow.yaml')

    # then
    self.assertEqual(Config.COM_port, "COM0")
    self.assertEqual(Config.COM_baud_rate, 9600)

  def test_load_config_file_withAdvancedMappings(self):
    # when
    Config.load_config_file('resources/testConfigYaml_advancedMappings.yaml')

    # then
    self.assertEqual(Config.keystroke_mappings.get('00'), ['b'])
    self.assertEqual(Config.keystroke_mappings.get('01'), ['a', ['shift', 'left'], ['ctrl', 'b'], 'c', ['ctrl', 'd']])

  def test_load_config_file_raisesExceptionWhenStrokeMappingIsNotList(self):
    Config.keystroke_mappings = {}

    # when
    with self.assertRaises(SyntaxError):
      Config.load_config_file('resources/testConfigYaml_nolistMappings.yaml')

    # then
    self.assertTrue('01' not in Config.keystroke_mappings.keys())
