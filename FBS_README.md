
### Setup

1. Create project setup
    ```bash
    fbs startproject
    ```

    ### Development    
1. Run project for development
    ```bash
    fbs run
    ```

    ### Build / export
1.  Include `fix_pyqt5_path.py` before you include PyQt5 in `main.py`:
    
    ```python
    # main.py
    import fix_pyqt5_path
    
    # other imports ...
    ```

1. Clean project target output directory
    ```bash
    fbs clean
    ```

1. Make sure output target directory has the right rights
    ```bash
    # Linux
    chmod 777 target/
    ```
    ```cmd
    # Windows
    icalcls "target" /grant Users:F /T
    ```

1. Build and export project
    ```bash
    fbs freeze --debug
    ```
    
    ### Installer
    
1. Install NSIS and add it to the path:
    ```cmd
    set PATH=%PATH%;D:\Programs\NSIS
    ```
    
1. Run fbs installer
    ```cmd
    fbs installer
    ```