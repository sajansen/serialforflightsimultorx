import logging
import time
from enum import Enum

logger = logging.getLogger(__name__)


def setup_logging():
  from config import Config
  logging.basicConfig(level=Config.logging_level,
                      format=Config.logging_format)


def wait_and_print(seconds):
  current_second = 0
  while current_second < seconds:
    logger.debug("%s... ", seconds - current_second)
    time.sleep(1)
    current_second += 1


def check_if_key_exists_in_dict_and_is_dict(hay, needle):
  return needle in hay.keys() and isinstance(hay[needle], dict)


def check_if_key_exists_in_dict_and_is_list(hay, needle):
  return needle in hay.keys() and isinstance(hay[needle], list)


class AutoNameEnum(Enum):
  def __new__(cls):
    value = len(cls.__members__)  # note no + 1
    obj = object.__new__(cls)
    obj._value_ = value
    return obj
