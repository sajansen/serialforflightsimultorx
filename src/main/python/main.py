import fix_pyqt5_path   # DO NOT REMOVE THIS IMPORT

import logging
import sys

from fbs_runtime.application_context.PyQt5 import ApplicationContext

import utils
from config import Config
from trayicon import SystemTraiIcon

logger = logging.getLogger(__name__)


def main(argv):
  Config.setup_config()

  app_context = ApplicationContext()
  tray_icon = SystemTraiIcon(app_context)
  tray_icon.show()
  sys.exit(app_context.app.exec_())


if __name__ == "__main__":
  main(sys.argv[1:])
