import logging

import pyautogui
import serial
from pymsgbox import alert

from config import Config
from fsx import FSX
from status import statuses_to_bytes, get_status_params, StatusUpdateMode, get_bytetype_byte_length, \
  Status, bytes_to_status_value

logger = logging.getLogger(__name__)


def get_strokes_for_value(value):
  strokes = Config.keystroke_mappings.get(value)

  if not strokes:
    raise IndexError("No strokes defined for value: {} / {}".format(value, hex(value)))

  return strokes


def execute_strokes(strokes):
  logger.info("Executing strokes: %s", strokes)
  for stroke in strokes:
    if isinstance(stroke, list):
      pyautogui.hotkey(*stroke)
    else:
      pyautogui.press(stroke)


def get_status_for_value(value):
  status = Config.serial_to_simulation_statuses.get(value)

  if status is None:
    raise IndexError("No status defined for value: {} / {}".format(value, hex(value)))

  return status


def send_update_to_serial(serial_device: serial.Serial, simulator: FSX):
  # Get status values
  try:
    values = simulator.get_status()
  except Exception as exception:
    logger.error("Failed to retrieve simulator data")
    logger.exception(exception, exc_info=True)
    return

  # Transform status into bytes
  try:
    total_bytes = statuses_to_bytes(values)
  except Exception as exception:
    logger.error("Failed to convert status to bytes")
    logger.debug(values)
    logger.exception(exception, exc_info=True)
    return

  # Send bytes
  serial_device.write(total_bytes)
  logger.debug("Serial update send with values: %s", values)


def serial_read_value_for_status(serial_device: serial.Serial, status: Status):
  status_params = get_status_params(status)

  byte_length = get_bytetype_byte_length(status_params['byte_type'])
  byte_value = serial_device.read(byte_length)
  value = bytes_to_status_value(status, byte_value)
  logger.debug("Received new value: %s -> %s", byte_value, value)

  return value


def interact_with_serial_port():
  Config.setup_config()
  simulator = FSX()
  simulator.connect()

  with serial.Serial(Config.COM_port, Config.COM_baud_rate) as ser:
    logger.info("Connected to serial port: %s", ser.name)
    ser.reset_input_buffer()

    while True:
      # Serial -> Simulator
      value = ord(ser.read())

      if value == Config.COM_needs_status_update_byte:
        send_update_to_serial(serial_device=ser, simulator=simulator)
        continue

      try:
        status = get_status_for_value(value)
        status_params = get_status_params(status)

        if status_params.get('update_mode') == StatusUpdateMode.TOGGLE:
          simulator.toggle_status(status=status)

        elif status_params.get('update_mode') == StatusUpdateMode.BIT_TOGGLE:
          simulator.toggle_status_bit(status=status, bit_position=status_params.get('bit_offset'))

        elif status_params.get('update_mode') == StatusUpdateMode.BIT_REPLACE:
          new_status_value = serial_read_value_for_status(ser, status)
          simulator.set_status_bit(status=status, bit_position=status_params.get('bit_offset'), value=new_status_value)

        elif status_params.get('update_mode') == StatusUpdateMode.REPLACE:
          new_status_value = serial_read_value_for_status(ser, status)
          simulator.set_status(status=status, value=new_status_value)

      except IndexError as exception:
        # logger.info(exception)
        pass
      except Exception as exception:
        logger.warning("Could not process serial value for status update: %s / %s", value, hex(value))
        logger.exception(exception, True)

      try:
        strokes = get_strokes_for_value(value)
        execute_strokes(strokes)
      except IndexError as exception:
        # logger.info(exception)
        pass
      except Exception as exception:
        logger.warning("Could not process serial value for stoke mapping: %s / %s", value, hex(value))
        logger.exception(exception, True)


def interact_with_serial_port_test(on_error=None):
  Config.setup_config()

  left_toggle = False
  right_toggle = False

  try:
    with serial.Serial(Config.COM_port, Config.COM_baud_rate) as ser:
      logger.info("Connected to serial port: %s", ser.name)
      ser.reset_input_buffer()

      while True:
        value = ser.read().hex()

        if value not in ['12', '13']:
          continue

        if value == '12':
          left_toggle = not left_toggle
          if not left_toggle:
            continue
        if value == '13':
          right_toggle = not right_toggle
          if not right_toggle:
            continue

        if left_toggle:
          # win32clipboard.OpenClipboard()
          # data = win32clipboard.GetClipboardData()
          # win32clipboard.CloseClipboard()
          execute_strokes([
            'tab',
            'tab',
            'tab',
            'tab',
            'tab',
            'tab',
            'tab',
            'tab',
            'tab',
            '1',
            '0',
            'enter',
          ])

        if right_toggle:
          execute_strokes(['enter'])


        continue
        try:
          strokes = get_strokes_for_value(value)
          execute_strokes(strokes)
        except IndexError as exception:
          logger.info(exception)
        except Exception as exception:
          logger.warning("Could not process serial value for stoke mapping: 0x%s", value)
          logger.exception(exception, True)
  except:
    if on_error is not None:
      on_error()


def identify_serial_values():
  Config.setup_config()

  with serial.Serial(Config.COM_port, Config.COM_baud_rate) as ser:
    logger.info("Connected to serial port: %s", ser.name)
    ser.reset_input_buffer()

    while True:
      value = ord(ser.read())

      if value == Config.COM_needs_status_update_byte:
        ser.write(1)
        logger.debug("Serial update send with values: %s", 0)
        continue

      logger.info("Identified value: %s / %s", value, hex(value))
      alert(text="{} / {}".format(value, hex(value)), title='Identified value')


if __name__ == "__main__":
  interact_with_serial_port()
