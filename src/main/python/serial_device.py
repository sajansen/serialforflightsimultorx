import logging

from config import Config
from status import get_bytes_for_status, get_byte_length_for_all_statuses

logger = logging.getLogger(__name__)


def serial_send_status(serial_device, status):
  for i, status in enumerate(status):
    serial_device.write(get_bytes_for_status(status))
  serial_device.write(Config.COM_close_char)


def serial_read_status(serial_device):
  total_bytes_length = get_byte_length_for_all_statuses(Config.serial_to_simulation_statuses)
  status = serial_device.read(total_bytes_length)
  logger.debug("Received values from serial: %s", status)

  serial_device.reset_input_buffer()

  pass
