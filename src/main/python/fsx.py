import logging
import sys
import time

import pyuipc
from config import Config
from status import create_simulation_offsets_for_statuses, Status, get_status_params, \
  status_params_to_simulator_offset, is_status_a_bit_value
from utils import wait_and_print

logger = logging.getLogger(__name__)


class FSX:
  def __init__(self):
    self.pyuipcConnection = None

    self.pyuipc_read_offsets = None
    self.read_statuses = Config.simulation_to_serial_statuses
    self.read_offsets = create_simulation_offsets_for_statuses(self.read_statuses)

    self.pyuipc_write_offsets = None
    self.write_statuses = list(Config.serial_to_simulation_statuses.values())
    self.write_offsets = create_simulation_offsets_for_statuses(self.write_statuses)

  def connect(self):
    while True:
      try:
        self.pyuipcConnection = pyuipc.open(pyuipc.SIM_ANY)
        self.pyuipc_write_offsets = pyuipc.prepare_data(self.write_offsets)
        self.pyuipc_read_offsets = pyuipc.prepare_data(self.read_offsets)
        logger.info("Connected to simulator")
        break
      except NameError:
        logger.error('Using voiceAtis without FSUIPC.')
        break
      except:
        logger.error('FSUIPC: No simulator detected. Start your simulator first! Retrying in 5 seconds.')
        wait_and_print(5)

  def set_status(self, status: Status, value):
    offsets = create_simulation_offsets_for_statuses([status])
    pyuipc_offset = pyuipc.prepare_data(offsets)

    pyuipc.write(pyuipc_offset, [value])
    logger.info("Status %s updated to %s", status.name, value)

  def toggle_status(self, status: Status):
    offsets = create_simulation_offsets_for_statuses([status])
    pyuipc_offset = pyuipc.prepare_data(offsets)

    current_value = pyuipc.read(pyuipc_offset)[0]
    current_value = int(not current_value)

    pyuipc.write(pyuipc_offset, [current_value])
    logger.info("Status %s toggled to %s", status.name, current_value)

  def set_status_bit(self, status: Status, bit_position: int, value: int):
    offsets = create_simulation_offsets_for_statuses([status])
    pyuipc_offset = pyuipc.prepare_data(offsets)

    current_value = pyuipc.read(pyuipc_offset)[0]
    if value:
      current_value |= 1 << bit_position
    else:
      current_value &= ~(1 << bit_position) & 0xffff

    pyuipc.write(pyuipc_offset, [current_value])
    logger.info("Status %s toggled to %s", status.name, bin(current_value))

  def toggle_status_bit(self, status: Status, bit_position: int):
    offsets = create_simulation_offsets_for_statuses([status])
    pyuipc_offset = pyuipc.prepare_data(offsets)

    current_value = pyuipc.read(pyuipc_offset)[0]
    current_value = current_value ^ 1 << bit_position

    pyuipc.write(pyuipc_offset, [current_value])
    logger.info("Status %s toggled to %s", status.name, bin(current_value))

  def get_status(self):
    # Split read statuses into bit- and non-bit statuses including their status_params (details called here,
    # because we're gonna at some extra fields later on)
    non_bit_statuses_details = {}
    bit_statuses_details = {}
    for status in self.read_statuses:
      status_params = get_status_params(status)

      if is_status_a_bit_value(status):
        bit_statuses_details[status] = status_params
      else:
        non_bit_statuses_details[status] = status_params

    # Get FS values for the non-bit statuses
    self.get_status_values_for_non_bit_status_details(non_bit_statuses_details)

    # Get FS values for the bit statuses
    self.get_status_values_for_bit_status_details(bit_statuses_details)

    # Combine and map data to initial read status list
    output_values = []
    for status in self.read_statuses:
      if status in bit_statuses_details.keys():
        output_values.append(bit_statuses_details.get(status).get('value'))
      else:
        output_values.append(non_bit_statuses_details.get(status).get('value'))

    return output_values

  def get_status_values_for_bit_status_details(self, bit_statuses_details):
    # Combine all bit statuses with the same address
    combined_bit_statuses_details = {}
    for bit_status_detail in bit_statuses_details.values():
      if bit_status_detail.get('address') in combined_bit_statuses_details.keys():
        continue
      combined_bit_statuses_details[bit_status_detail.get('address')] = bit_status_detail.get('byte_type')

    # Get FS data for each bit-statuses and extract the bit from it
    for address, byte_type in combined_bit_statuses_details.items():
      pyuipc_offset = pyuipc.prepare_data([status_params_to_simulator_offset({'address': address, 'byte_type': byte_type})])
      value = pyuipc.read(pyuipc_offset)[0]

      for status_detail in bit_statuses_details.values():
        if status_detail.get('address') != address:
          continue

        status_detail['value'] = (value >> status_detail.get('bit_offset')) & 1

  def get_status_values_for_non_bit_status_details(self, non_bit_statuses_details):
    non_bit_statuses_offsets = [status_params_to_simulator_offset(status_param) for status_param in
                                non_bit_statuses_details.values()]
    pyuipc_offset = pyuipc.prepare_data(non_bit_statuses_offsets)
    non_bit_statuses_values = pyuipc.read(pyuipc_offset)

    # Put the received values back into the details dict
    for i, status_detail in enumerate(non_bit_statuses_details.values()):
      status_detail['value'] = non_bit_statuses_values[i]
