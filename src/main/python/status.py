from enum import Enum

from utils import AutoNameEnum


class StatusUpdateMode(Enum):
    READ_ONLY = 0
    TOGGLE = 1
    REPLACE = 2
    BIT_TOGGLE = 3
    BIT_REPLACE = 4


##
# Add below new statuses with their offsets
##


class Status(AutoNameEnum):
    # Lights
    NAVIGATION_LIGHTS = ()
    BEACON_LIGHTS = ()
    LANDING_LIGHTS = ()
    TAXI_LIGHTS = ()
    STROBE_LIGHTS = ()
    PANEL_LIGHTS = ()
    RECOGNITION_LIGHTS = ()
    WING_LIGHTS = ()
    LOGO_LIGHTS = ()
    CABIN_LIGHTS = ()

    # Failures
    PANEL_FAILURE_COMPASS = ()

    # Controls
    SPOILER_CONTROL = ()
    RUDDER_POSITION_CONTROL = ()
    BRAKE_LEFT = ()
    BRAKE_RIGHT = ()
    FLAPS_INDEX = ()
    FLAPS_DETENT_INCREMENT = ()

    # Aircraft information
    GROUND_SPEED = ()
    INDICATED_AIR_SPEED = ()
    MAX_AIRSPEED = ()
    OVERSPEED_WARNING = ()
    GEAR_TYPE = ()
    GEAR_ACTUAL_NOSE = ()
    GEAR_ACTUAL_RIGHT = ()
    GEAR_ACTUAL_LEFT = ()
    GROUND_ALTITUDE = ()

    # Simulator information
    PLANE_ON_GROUND = ()
    CRASHED = ()
    PAUSE_FLAG = ()

    # Environment
    CLOUD_TURBULENCE_AT_AIRCRAFT = ()


status_params = {
    # Lights
    Status.NAVIGATION_LIGHTS: {'address': 0x0D0C, 'byte_type': 'S16', 'update_mode': StatusUpdateMode.BIT_TOGGLE,
                               'bit_offset': 0},
    Status.BEACON_LIGHTS: {'address': 0x0D0C, 'byte_type': 'S16', 'update_mode': StatusUpdateMode.BIT_TOGGLE,
                           'bit_offset': 1},
    Status.LANDING_LIGHTS: {'address': 0x0D0C, 'byte_type': 'S16', 'update_mode': StatusUpdateMode.BIT_TOGGLE,
                            'bit_offset': 2},
    Status.TAXI_LIGHTS: {'address': 0x0D0C, 'byte_type': 'S16', 'update_mode': StatusUpdateMode.BIT_TOGGLE,
                         'bit_offset': 3},
    Status.STROBE_LIGHTS: {'address': 0x0D0C, 'byte_type': 'S16', 'update_mode': StatusUpdateMode.BIT_TOGGLE,
                           'bit_offset': 4},
    Status.PANEL_LIGHTS: {'address': 0x0D0C, 'byte_type': 'S16', 'update_mode': StatusUpdateMode.BIT_TOGGLE,
                          'bit_offset': 5},
    Status.RECOGNITION_LIGHTS: {'address': 0x0D0C, 'byte_type': 'S16', 'update_mode': StatusUpdateMode.BIT_TOGGLE,
                                'bit_offset': 6},
    Status.WING_LIGHTS: {'address': 0x0D0C, 'byte_type': 'S16', 'update_mode': StatusUpdateMode.BIT_TOGGLE,
                         'bit_offset': 7},
    Status.LOGO_LIGHTS: {'address': 0x0D0C, 'byte_type': 'S16', 'update_mode': StatusUpdateMode.BIT_TOGGLE,
                         'bit_offset': 8},
    Status.CABIN_LIGHTS: {'address': 0x0D0C, 'byte_type': 'S16', 'update_mode': StatusUpdateMode.BIT_TOGGLE,
                          'bit_offset': 9},

    # Failures
    Status.PANEL_FAILURE_COMPASS: {'address': 0x3BDC, 'byte_type': 'U8', 'update_mode': StatusUpdateMode.TOGGLE},

    # Controls
    Status.SPOILER_CONTROL: {'address': 0x0BD0, 'byte_type': 'S32', 'update_mode': StatusUpdateMode.REPLACE},
    Status.RUDDER_POSITION_CONTROL: {'address': 0x0BBA, 'byte_type': 'S16', 'update_mode': StatusUpdateMode.REPLACE},
    Status.BRAKE_LEFT: {'address': 0x0BC4, 'byte_type': 'S16', 'update_mode': StatusUpdateMode.REPLACE},
    Status.BRAKE_RIGHT: {'address': 0x0BC6, 'byte_type': 'S16', 'update_mode': StatusUpdateMode.REPLACE},
    Status.FLAPS_INDEX: {'address': 0x0BFC, 'byte_type': 'U8', 'update_mode': StatusUpdateMode.REPLACE},
    Status.FLAPS_DETENT_INCREMENT: {'address': 0x3BFA, 'byte_type': 'U16'},

    # Aircraft information
    Status.GROUND_SPEED: {'address': 0x02b4, 'byte_type': 'S32'},
    Status.INDICATED_AIR_SPEED: {'address': 0x02BC, 'byte_type': 'S32'},
    Status.MAX_AIRSPEED: {'address': 0x02C4, 'byte_type': 'U32'},
    Status.OVERSPEED_WARNING: {'address': 0x036D, 'byte_type': 'U8'},
    Status.GEAR_TYPE: {'address': 0x060C, 'byte_type': 'U16'},
    Status.GEAR_ACTUAL_NOSE: {'address': 0x0BEC, 'byte_type': 'S32'},
    Status.GEAR_ACTUAL_RIGHT: {'address': 0x0BF0, 'byte_type': 'S32'},
    Status.GEAR_ACTUAL_LEFT: {'address': 0x0BF4, 'byte_type': 'S32'},
    Status.GROUND_ALTITUDE: {'address': 0x0B4C, 'byte_type': 'S16'},

    # Simulator information
    Status.PLANE_ON_GROUND: {'address': 0x0366, 'byte_type': 'U16'},
    Status.CRASHED: {'address': 0x0840, 'byte_type': 'S16'},
    Status.PAUSE_FLAG: {'address': 0x0264, 'byte_type': 'S16'},

    # Environment
    Status.CLOUD_TURBULENCE_AT_AIRCRAFT: {'address': 0x0E88, 'byte_type': 'U16'},
}

##
#                ------------- end -------------
##

#  - b: U8      a 1-byte unsigned value, to be converted into a Python int
#  - c: S8      a 1-byte signed value, to be converted into a Python int
#  - h: U16     a 2-byte signed value, to be converted into a Python int
#  - H: S16     a 2-byte unsigned value, to be converted into a Python int
#  - d: U32     a 4-byte signed value, to be converted into a Python int
#  - u: S32     a 4-byte unsigned value, to be converted into a Python long
#  - l: U64     an 8-byte signed value, to be converted into a Python long
#  - L: S64     an 8-byte unsigned value, to be converted into a Python long
#  - f: FLT64   an 8-byte floating point value, to be converted into a Python double
simulation_offset_map = {
    'U8': 'b',
    'S8': 'c',
    'U16': 'h',
    'S16': 'H',
    'U32': 'd',
    'S32': 'u',
    'U64': 'l',
    'S64': 'L',
    'FLT64': 'f',
}


def status_params_to_simulator_offset(status_params: dict) -> tuple:
    address = status_params['address']
    byte_type = simulation_offset_map.get(status_params['byte_type'])

    if byte_type is None:
        raise IndexError("No simulation offset defined for status offset: {}".format(status_params))

    return address, byte_type


def create_simulation_offsets_for_statuses(statuses: list) -> list:
    offsets = []
    for status in statuses:
        status_params = get_status_params(status)

        simulation_offset = status_params_to_simulator_offset(status_params)
        offsets.append(simulation_offset)
    return offsets


def get_status_params(status: Status):
    status_offset = status_params.get(status)
    if status_offset is None:
        raise IndexError("No offset defined for status: {}".format(status))
    return status_offset


def is_status_a_bit_value(status: Status):
    return True if get_status_params(status).get('bit_offset', None) is not None else False


def status_valus_as_bytes(status: Status, value: int):
    from config import Config

    if is_status_a_bit_value(status):
        signed = False
        length = 1
    else:
        params = get_status_params(status)
        signed = is_bytetype_signed(params['byte_type'])
        length = get_bytetype_byte_length(params['byte_type'])

    return value.to_bytes(length, byteorder=Config.COM_read_byte_order, signed=signed)


def bytes_to_status_value(status: Status, byte_value: bytearray):
    from config import Config

    params = get_status_params(status)
    signed = is_bytetype_signed(params['byte_type'])

    return int.from_bytes(byte_value, byteorder=Config.COM_write_byte_order, signed=signed)


def is_bytetype_signed(byte_type):
    return byte_type[0] == "S"


def get_bytetype_byte_length(byte_type):
    return int(int(byte_type[1:]) / 8)


def statuses_to_bytes(values):
    from config import Config

    total_bytes = bytearray()
    for i, status in enumerate(Config.simulation_to_serial_statuses):
        status_bytes = status_valus_as_bytes(status, values[i])
        total_bytes += status_bytes
    return total_bytes
