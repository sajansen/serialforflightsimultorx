# This class contains all the settings for running the project. Edits can be made here.
import logging
import os
import yaml

from status import Status
from utils import check_if_key_exists_in_dict_and_is_dict, check_if_key_exists_in_dict_and_is_list, \
  setup_logging

logger = logging.getLogger(__name__)


class Config:
  ##
  # Serial communication
  ##

  COM_needs_status_update_byte = 0xff
  COM_port = 'COM14'
  COM_baud_rate = 9600
  COM_read_byte_order = 'little'
  COM_write_byte_order = 'big'

  ##
  # Logging
  ##

  # Logging level as one of Python logging levels (DEBUG, INFO, WARNING, ERROR, ...)
  logging_level = "DEBUG"
  logging_format = "[%(asctime)s | %(name)s | %(levelname)-8s] %(message)s"

  ##
  # Default stroke mappings
  ##

  # Note: Keep in mind the reserved byte for Config.COM_needs_status_update_byte

  # Each mapping consists of a byte read from the serial port as the dictonary key
  # and the to-be-executed strokes as the value. This value must be a list of
  # key presses and/or key strokes (sublists).
  keystroke_mappings = {}

  serial_to_simulation_statuses = {}

  simulation_to_serial_statuses = []

  ##
  # GUI
  ##

  GUI_trayIconDefault = "icons/trayicon.png"
  GUI_trayIconRunning = "icons/trayicon_running.png"
  GUI_trayIconError = "icons/trayicon_error.png"
  GUI_startIcon = "icons/start-32.png"
  GUI_stopIcon = "icons/stop-32.png"
  GUI_exitIcon = "icons/exit-32.png"
  GUI_questionIcon = "icons/question-32.png"

  @staticmethod
  def setup_config(config_file_path=None):
    if config_file_path is None:
      self_dir = os.path.dirname(os.path.realpath(__file__))
      config_file_path = os.path.join(self_dir, 'config.yaml')

    if os.path.isfile(config_file_path):
      Config.load_config_file(config_file_path)

  @staticmethod
  def load_config_file(file_path):
    logger.debug("Loading config file: %s", file_path)
    with open(file_path, 'r') as stream:
      yaml_config = yaml.safe_load(stream)

    if 'logging' in yaml_config.keys():
      Config.logging_level = yaml_config['logging'].get('level', Config.logging_level)
      Config.logging_format = yaml_config['logging'].get('format', Config.logging_format)
      setup_logging()

    if 'serial' in yaml_config.keys():
      Config.COM_port = yaml_config['serial'].get('port', Config.COM_port)
      Config.COM_baud_rate = yaml_config['serial'].get('baud_rate', Config.COM_baud_rate)

    if 'mappings' in yaml_config.keys():
      if check_if_key_exists_in_dict_and_is_dict(yaml_config['mappings'], 'keystrokes'):
        for mapping_key, mapping_value in yaml_config['mappings']['keystrokes'].items():

          if not isinstance(mapping_value, list):
            raise SyntaxError("Keytroke mappings must contain a list as values for each mapping. '{}' is not a list!"
                              .format(mapping_value))

          key = Config.string_key_to_int_key(mapping_key)

          if key == Config.COM_needs_status_update_byte:
            raise KeyError("Cannot use key '{}' for a keystroke mapping. This key is reserved!"
                           .format(Config.COM_needs_status_update_byte))

          Config.keystroke_mappings[key] = mapping_value
          logger.debug("Loaded keystroke mapping: %s / %s -> %s", key, hex(key), mapping_value)

      if check_if_key_exists_in_dict_and_is_dict(yaml_config['mappings'], 'events'):
        if check_if_key_exists_in_dict_and_is_dict(yaml_config['mappings']['events'], 'incoming'):
          for mapping_key, mapping_value in yaml_config['mappings']['events']['incoming'].items():

            if not isinstance(mapping_value, str):
              raise SyntaxError("Incoming event mappings must have a string as value. '{}' is not a string!"
                                .format(mapping_value))

            key = Config.string_key_to_int_key(mapping_key)

            if key == Config.COM_needs_status_update_byte:
              raise KeyError("Cannot use key '{}' for a incoming event mapping. This key is reserved!"
                             .format(Config.COM_needs_status_update_byte))

            Config.serial_to_simulation_statuses[key] = Status[mapping_value]
            logger.debug("Loaded incoming event mapping: %s / %s -> %s", key, hex(key), Config.serial_to_simulation_statuses[key].name)

        if check_if_key_exists_in_dict_and_is_list(yaml_config['mappings']['events'], 'outgoing'):
          for mapping_value in yaml_config['mappings']['events']['outgoing']:

            if not isinstance(mapping_value, str):
              raise SyntaxError("Outgoing event mappings must have a string as value. {} is not a string!"
                                .format(mapping_value))

            Config.simulation_to_serial_statuses.append(Status[mapping_value])
            logger.debug("Loaded outgoing event mapping: %s", Config.simulation_to_serial_statuses[-1].name)

  @staticmethod
  def string_key_to_int_key(mapping_key):
    if isinstance(mapping_key, str) and "0x" in mapping_key:
      key = Config.hex_string_to_key(mapping_key)
    else:
      key = int(mapping_key)
    return key

  @staticmethod
  def hex_string_to_key(string):
    return int(string[2:], 16)    # Strip '0x'
