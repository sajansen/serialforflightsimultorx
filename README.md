# SerialForFlightSimultorX

_Exchange data between Flight Simulator and a serial device, like an Arduino._

## Setup

#### FSUIPC

Install the required libraries for communication between this program and your flight simulator from [FSUIPC](http://www.schiratti.com/dowson.html). You'll need the developer library [FSUIPC SDK](http://fsuipc.simflight.com/beta/FSUIPC_SDK.zip) and your specifiv FSUIPC 4 or 5 library. For my flight simulator (X) I needed [FSUIPC4 4.971](http://fsuipc.simflight.com/beta/FSUIPC4.zip). 

Extract the developer library FSUIPC_SDK and navigate to the subfolder/zip _FSUIPC_SDK/UIPC_SDK_Python/_. Execute _pyuipc-0.4.win32-py3.7.msi_ to install the library for Python 3.7 development. Use your current Python version. This must be a 32-bit Python version, because the 64-bit Python won't recognise pyuipc. 

### Config
Edit the serial port settings in the `config.yaml` file as desired. You can create a `config.yaml` by copying [`config.yaml.example`](src/main/python/config.yaml.example) and renaming it to `config.yaml`. 

#### Config file scheme

##### Serial
Configure serial settings, like port and baud rate.

##### Logging
Configure additional logging settings

##### Keystroke mappings
Under `strokes.mappings`, keystroke mappings can be added. This mapping is done by specifying the serial input byte as the key (in hex notation). The value is the to be pressed keys, as a list. 
```yaml
strokes:
  mappings:
    '0x00':   # Pressing 'a'
      - a
```

For more complex mappings, multidimensional listing can be used, like this:
```yaml
strokes:
  mappings:
    '0x00':   # Pressing, selecting, and copying a character 'a'
      - a
      - [shift, left]     # Specify a hotkey as this (preferred notation for readability)
      - - ctrl            # Or as this
        - c
```

<b>Important:</b> the identifier `0xff` is reserved for incoming data request from the serial device.

<b>Note:</b> you can also edit and run the [config_yaml_mappings_creator](tools/config_yaml_mappings_creator.py) from the [tools](tools) directory if you are unfamiliar with YAML. Just edit the dictionary, run the script and copy and paste the output in your config.yaml.

On each element of the list, a [keyPress](https://pyautogui.readthedocs.io/en/latest/keyboard.html#the-press-keydown-and-keyup-functions) action will be performed. If a element is a (sub)list, a [hotKey](https://pyautogui.readthedocs.io/en/latest/keyboard.html#the-hotkey-function) action will be performed with  each element of the sublist in the given order. 


### Run
Just run [`main.py`](src/main/python/main.py) to start the application. A tray icon will be created. Right click on this icon and you can _start_ listening to the serial device. You can also start the serial input _identifier_. It will directly display the received serial value. You can use this value for creating mappings.

The serial port will be closed with either the _Stop_ or _Exit_ action.


## Dependencies

* [pySerial](https://pyserial.readthedocs.io/en/latest/index.html) - for communication with the serial device
* [PyAutoGUI](https://pyautogui.readthedocs.io/en/latest/index.html) - for keyboard/mouse control
* [PyYAML](https://pyyaml.org/wiki/PyYAMLDocumentation) - for loading configuration
* [PyQt5](https://pypi.org/project/PyQt5/) - for rendering GUI's