                FSUIPC SDK for Python
                =====================

          by István Váradi (ivaradi@gmail.com)


This package contains the following files:

- pyuipc-0.4.win32-py<version>.msi: the installers of the Python extension module.

  They expect an existing installation for the same version of
  Python as denoted by <version>. Modules are provided only for
  32-bit versions of the Python runtime.

- src: a directory containing the sources:
  - pyuipc.cc: the extension module's implementation
  - setup.py: the setup script for Distutils.
  - Makefile: a sample Makefile to build the module. It works on Linux
    with the Mingw32 cross-compiler, but on other platforms it can be
    used as an example of how to build the extension.

- README.txt: this file

The API
-------

The SDK is a single module called "pyuipc". It was written entirely in
C++, so in the following we describe how the interface would look like
if it were Python code.

A number of simulator constants are defined, just as in the C API, and
with the same meaning:

SIM_ANY = 0
SIM_FS98 = 1
SIM_FS2K = 2
SIM_CFS2 = 3
SIM_CFS1 = 4
SIM_FLY = 5
SIM_FS2K2 = 6
SIM_FS2K4 = 7
SIM_FSX = 8
SIM_ESP = 9
SIM_P3D = 10
SIM_FSX64 = 11
SIM_P3D64 = 12

The error codes are defined similarly:

ERR_OK = 0
ERR_OPEN = 1
ERR_NOFS = 2
ERR_REGMSG = 3
ERR_ATOM = 4
ERR_MAP = 5
ERR_VIEW = 6
ERR_VERSION = 7
ERR_WRONGFS = 8
ERR_NOTOPEN = 9
ERR_NODATA = 10
ERR_TIMEOUT = 11
ERR_SENDMSG = 12
ERR_DATA = 13
ERR_RUNNING = 14
ERR_SIZE = 15

The functions raise an exception called FSUIPCException:

class FSUIPCException(Exception):
    def __init__(self, errorCode):
        self.errorCode = errorCode
        self.errorString = errorStrings[errorCode]
        Exception.__init__(self, self.errorString)

The 'errorCode' member contains one of the error codes above, while
'errorString' is a textual representation of the error, which is also
returned when converting the exception to a string.

The module defines the following functions:

- open(version)

  Open a connection to the simulator. 'version' is one of the SIM_xxx
  constants above. If a connection could be established, the function
  returns normally and the following variables will be define in the
  module:

  - fs_version: the actual version of the simulator, also one of the
    SIM_xxx constants above

  - fsuipc_version: the FSUIPC version as encoded in the C API
    ((1000*version number)<<16 | build number)

  - lib_version: the version of the FSUIPC client library, encoded as
    fsuipc_version

  If the connection cannot be established, an FSUIPCException is
  thrown with the error code.

- prepare_data(data, forReading)

  Prepare a data specification for reading or writing. It may be
  necessary to repeatedly read from or write to a certain set of
  offsets. To avoid the cost of converting the sets of offsets and
  their types every time, you can prepare such sets in advance and
  reuse the later in actual read or write calls.

  'data' should be a list of tuples of two items. The first item of
  each tuple should be an offset to read or write. The second item
  denotes the type of the value to be read from or written into the
  offset. it should be either a string or an integer. If it is a
  string, it denotes a type of a fixed size:

  - b: a 1-byte unsigned value, to be converted into a Python int
  - c: a 1-byte signed value, to be converted into a Python int
  - h: a 2-byte signed value, to be converted into a Python int
  - H: a 2-byte unsigned value, to be converted into a Python int
  - d: a 4-byte signed value, to be converted into a Python int
  - u: a 4-byte unsigned value, to be converted into a Python long
  - l: an 8-byte signed value, to be converted into a Python long
  - L: an 8-byte unsigned value, to be converted into a Python long
  - f: an 8-byte floating point value, to be converted into a Python double

  If the second item is an integer, it denotes a string. If the value
  is positive, the string is of a fixed length with no zero
  terminator. If the value is negative, its absolute value is the
  maximal length of the string, but the string itself is zero
  terminated.

  'forReading' is a boolean value indicating if the data is prepared
  for reading or not. If omitted or True, the data will be prepared
  for reading and writing, otherwise only for writing.

  This function returns an opaque object that can be used in later
  calls to refer to the data prepared. If there is something wrong in
  the specification a TypeError exception is thrown with a message
  containing the description of the problem.

- read(data)

  Read the given data from FSUIPC. 'data' is either a prepared data
  object returned by prepare_data() or an offset-type specification as
  required by prepare_data().

  The function returns a list of the data items read.

  If an error occurs, a TypeError or an FSUIPCException is thrown.

- write(data):

  Write the given data to FSUIPC. 'data' is a list of tuples of three
  items. The first two are the offset and the data type as described
  for prepare_data(). The third item is the actual data to write at
  the given offset.

  If an error occurs, a TypeError or an FSUIPCException is thrown.

- write(preparedData, data)

  Write data based on a prepared data object.

  'preparedData' is a prepared data object returned by
  prepare_data(). 'data' is a list of of data items, each
  containing the value for an offset in the prepared data.

  If an error occurs, a TypeError or an FSUIPCException is thrown.

- close()

  Close the connection to FSUIPC.

Revision history
================

0.1
---

* Initial release

0.2
---

* Fixed a bug that caused an incorrect value to be written when writing a
  signed 32-bit integer.

0.3
---

* Fixed a bug when writing byte array potentially containing zeroes.

0.4
---

* Added constances for the 64-bit versions of FSX and P3D
